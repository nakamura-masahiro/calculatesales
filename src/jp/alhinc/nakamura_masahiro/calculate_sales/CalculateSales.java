package jp.alhinc.nakamura_masahiro.calculate_sales;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class CalculateSales {
	
	public static void inputFile(
			String filePath, 
			Map<String, String> branchNames, 
			Map<String, Long> branchSales) {
		
		//支店定義ファイル読み込み
		BufferedReader br = null;
		try {
			File file = new File(filePath, "branch.lst");
			
			if(! file.exists()) {
				System.out.println("支店定義ファイルが存在しません");
				return;
			}
			
			FileReader fr = new FileReader(file);
			br = new BufferedReader(fr);
			
			String line;
			while((line = br.readLine()) != null) {
				
				//分割
				String[] items = line.split(",");
				
				if((items.length != 2) || (!items[0].matches("^[0-9]{3}$"))) {
					System.out.println("支店定義ファイルのフォーマットが不正です");
					return;
				}
				
				//保持
				branchNames.put(items[0], items[1]);
				branchSales.put(items[0], 0L);
			}
			
		} catch(IOException e) {
			System.out.println("エラーが発生しました。");
			return;
		} finally {
			if(br != null) {
				try {
					br.close();
				} catch(IOException e) {
					System.out.println("closeできませんでした。");
					return;
				}
			}
		}
	}
	
	
	public static void outputFile(
			String filePath, 
			Map<String, String> branchNames, 
			Map<String, Long> branchSales) {
		
		//支店別集計ファイル書き込み
		BufferedWriter bw = null;
		try {
			File file = new File(filePath, "branch.out");
			bw = new BufferedWriter(new FileWriter(file));
			
			for(String key : branchNames.keySet()) {
				bw.write(key + "," + branchNames.get(key) + "," + branchSales.get(key));
				bw.newLine();
			}
		} catch(IOException e) {
			System.out.println("予期せぬエラーが発生しました");
			return;
		} finally {
			try {
				if(bw != null) {
					bw.close();
				}
			} catch(IOException e) {
				System.out.println("予期せぬエラーが発生しました");
				return;
			}
		}
	}
	
	
	public static void main(String[] args) {
		//コマンドライン引数のチェック
		if(args.length != 1) {
			System.out.println("予期せぬエラーが発生しました");
			return;
		}
		
		Map<String, String> branchNames = new HashMap<>();
		Map<String, Long> branchSales = new HashMap<>();
		
		inputFile(args[0], branchNames, branchSales);
		
		
		//売上ファイル読み込み
		File[] files = new File(args[0]).listFiles();
		
		List<File> rcdFiles = new ArrayList<>();
		for(int i = 0; i < files.length; i++) {
			
			//売上ファイルの判定
			String fileName = files[i].getName();
			if(files[i].isFile() && fileName.matches("^[0-9]{8}.rcd$")) {
				
				//リストへの追加（保持）
				rcdFiles.add(files[i]);
			}
		}
		
		//ソートする
		Collections.sort(rcdFiles);
		
		for(int i = 0; i < rcdFiles.size() -1; i++) {
			int former = Integer.parseInt(rcdFiles.get(i).getName().substring(0,8));
			int latter = Integer.parseInt(rcdFiles.get(i + 1).getName().substring(0,8));
			
			if((latter - former) != 1){
				
				//連番チェック
				System.out.println("売上ファイル名が連番になっていません");
				return;
			}
		}
		
		BufferedReader br = null;
		for(int i = 0; i < rcdFiles.size(); i++) {
			try {
				br = new BufferedReader(new FileReader(rcdFiles.get(i)));
				
				List<String> fileContents = new ArrayList<>();
				String line;
				while((line = br.readLine()) != null) {
					fileContents.add(line);
				}
				
				String fileName = rcdFiles.get(i).getName();
				
				//行数チェック
				if(fileContents.size() != 2) {
					System.out.println(fileName + "のフォーマットが不正です");
					return;
				}
				
				//支店コード存在チェック
				if(! branchNames.containsKey(fileContents.get(0))) {
					System.out.println(fileName + "の支店コードが不正です");
					return;
				}
				
				//売上額が数字なのかチェック
				if(! fileContents.get(1).matches("^[0-9]+$")) {
					System.out.println("予期せぬエラーが発生しました");
					return;
				}
				
				//支店コード抽出
				String branchCode = fileContents.get(0);
				
				//売上額抽出
				long fileSale = Long.parseLong(fileContents.get(1));
				
				//加算
				Long saleAmount = branchSales.get(branchCode) + fileSale;
				
				if(saleAmount >= 10000000000L) {
					System.out.println("合計金額が10桁を超えました");
					return;
				}
				
				//保持
				branchSales.put(branchCode, saleAmount);
				
			} catch(IOException e) {
				System.out.println("予期せぬエラーが発生しました");
				return;
			} finally {
				if(br != null) {
					try {
						br.close();
					} catch(IOException e) {
						System.out.println("予期せぬエラーが発生しました");
						return;
					}
				}
			}
		}
		
		
		outputFile(args[0], branchNames, branchSales);
		
	}
}
